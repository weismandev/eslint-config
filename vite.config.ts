import path from 'path';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import dts from 'vite-plugin-dts';

export default defineConfig({
  base: './',
  plugins: [react(), dts()],
  build: {
    emptyOutDir: true,
    sourcemap: true,
    lib: {
      entry: path.resolve(__dirname, 'index.ts'),
      name: 'eslintConfig',
      formats: ['es', 'cjs', 'umd'],
      fileName: 'index',
    },
  },
});
